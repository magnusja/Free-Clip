#ifndef PLUGINEDITOR_H_INCLUDED
#define PLUGINEDITOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "DspFilters/Dsp.h"
#include "OtherLookAndFeel.h"
#include "AboutWindow.h"
#include "Constants.h"

//==============================================================================
/**
*/
class FreeClipAudioProcessorEditor  : public AudioProcessorEditor, private Timer, private Slider::Listener, private ButtonListener, private ComboBoxListener
{
public:
    FreeClipAudioProcessorEditor (FreeClipAudioProcessor&);
    ~FreeClipAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

	void timerCallback() override;

	ScopedPointer<LevelMeter> m_meter;
	ScopedPointer<LevelMeter> m_meterOut;

	ScopedPointer<AudioVisualiserComponent> m_avm;
	ScopedPointer<AudioVisualiserComponent> m_avmout;

	OtherLookAndFeel otherLookAndFeel;
	OtherLookAndFeel2 otherLookAndFeel2;

private:
	void sliderValueChanged(Slider* slider) override;
	void buttonClicked(juce::Button *) override;
	void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	void sliderDragStarted(Slider* slider) override;
	void sliderDragEnded(Slider* slider) override;

    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    FreeClipAudioProcessor& processor;

	Label m_lgain;
	Label m_loutGain;
	Label m_loversampling;
	Label m_lceiling;
	Label m_lsoftselect;
	Label m_lselectedsoft;
	Label m_lpost;
	Label m_lovrval;
	Label m_lgainval;
	Label m_loutval;

	Slider m_swavetype;
	Slider m_ceiling;
	Slider m_gain;
	Slider m_Soversample;
	Slider m_debugKnob;
	Slider m_Ssoftness;
	Slider m_outGain;

	ComboBox m_postMenu;

	LevelMeterLookAndFeel* m_lnf;
	LevelMeterLookAndFeel* m_lnf2;

	int m_prevovr = -1;

	const int cwidth = CWIDTH;
	const int cheight = CHEIGHT;

	Image m_hcpic;
	Image m_tanpic;
	Image m_cubpic;
	Image m_arcpic;
	Image m_algpic;
	Image m_quintpic;
	Image m_vennLogo;

	void initLogoParams();

	int m_logoHeight;
	int m_logoWidth;
	int m_logoDestX;
	int m_logoDestY;

	void drawBackground(int wavetype, Graphics& g);

	//bool m_paintTest = false;

	//int m_lineOrder = 0;

	TooltipWindow m_toolTipWindow;

	ScopedPointer<AboutWindow> aboutWindow;
	TextButton aboutButton;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (FreeClipAudioProcessorEditor)
};


#endif  // PLUGINEDITOR_H_INCLUDED
