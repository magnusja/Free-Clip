#include "PluginProcessor.h"
#include "PluginEditor.h"
//#include <Dense>
#define M_PI 3.14159265358979323846
#include "DspFilters/Dsp.h"

//==============================================================================
FreeClipAudioProcessor::FreeClipAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
	m_bufferResized = new AudioSampleBuffer(2,0);

	addParameter(p_gainf = new AudioParameterFloat("gain", "Gain", juce::Decibels::decibelsToGain(-20.0f), juce::Decibels::decibelsToGain(20.0f), 1.0f));
	addParameter(p_outGainf = new AudioParameterFloat("output", "Output", juce::Decibels::decibelsToGain(-20.0f), juce::Decibels::decibelsToGain(20.0f), 1.0f));
	addParameter(p_ceilingf = new AudioParameterFloat("ceiling", "Ceiling", juce::Decibels::decibelsToGain(-40.0f), juce::Decibels::decibelsToGain(0.0f), 1.0f));
	addParameter(p_wavetype = new AudioParameterInt("wavetype", "Wavetype", 0, 5, 0));
	addParameter(p_oversample = new AudioParameterInt("oversample", "Oversample", 0, 5, 0));
}

FreeClipAudioProcessor::~FreeClipAudioProcessor()
{
	delete m_bufferResized;
}

//==============================================================================
const String FreeClipAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool FreeClipAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool FreeClipAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

double FreeClipAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int FreeClipAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int FreeClipAudioProcessor::getCurrentProgram()
{
    return 0;
}

void FreeClipAudioProcessor::setCurrentProgram (int index)
{
}

const String FreeClipAudioProcessor::getProgramName (int index)
{
    return String();
}

void FreeClipAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void FreeClipAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
	m_prevgain = *p_gainf;
	m_prevceil = *p_ceilingf;
	m_prevoutGain = *p_outGainf;
	m_bufferResized->setSize(2, samplesPerBlock * maxOversample, false, true, false);
	m_bufferResized->clear();
	m_fsamplerate = sampleRate;

	f.setup(m_forder, m_fsamplerate*m_ioversample, calcCutoff());
	f2.setup(m_forder, m_fsamplerate*m_ioversample, calcCutoff());

	levelMeterSourceIn.resize(2, ceil((8192.0 / samplesPerBlock) * (sampleRate / 48000)));
	levelMeterSourceOut.resize(2, ceil((8192.0 / samplesPerBlock) * (sampleRate / 48000)));

	if (*p_oversample == 1)
		setLatencySamples(4);
	else if (*p_oversample > 1)
		setLatencySamples(5);
	else
		setLatencySamples(0);
}

void FreeClipAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool FreeClipAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

inline double fastatan(double x) {
	return (x / (1.0f + 0.28f * (x * x)));
}

void FreeClipAudioProcessor::processBlock(AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{

	if (m_settingsChanged || m_prevovr != *p_oversample)
	{
		m_ioversample = pow(2, static_cast<int>(*p_oversample));
		f.setup(m_forder, m_fsamplerate*m_ioversample, calcCutoff());
		f2.setup(m_forder, m_fsamplerate*m_ioversample, calcCutoff());
		m_prevovr = *p_oversample;
		f.reset();
		f2.reset();

		if (!m_settingsChanged)
		{
			if (*p_oversample == 1)
				setLatencySamples(4);
			else if (*p_oversample > 1)
				setLatencySamples(5);
			else
				setLatencySamples(0);
		}

		m_settingsChanged = false;
	}

	m_bufferResized->setSize(2, buffer.getNumSamples() * m_ioversample, false, true, true);
	m_bufferResized->clear();

	const int totalNumInputChannels  = getTotalNumInputChannels();
	const int totalNumOutputChannels = getTotalNumOutputChannels();

	const int numchans = buffer.getNumChannels();


	if (*p_gainf != m_prevgain)
	{
		buffer.applyGainRamp(0, buffer.getNumSamples(), m_prevgain, *p_gainf);
		m_prevgain = *p_gainf;
	}
	else
	{
		buffer.applyGain(*p_gainf);
	}

	levelMeterSourceIn.measureBlock(buffer);

	if (getActiveEditor() != nullptr)
	{
		static_cast<FreeClipAudioProcessorEditor*>(getActiveEditor())->m_avm->pushBuffer(buffer);
	}

	if (*p_ceilingf != m_prevceil)
	{
		buffer.applyGainRamp(0, buffer.getNumSamples(), 1.0f / m_prevceil, 1.0f / *p_ceilingf);
	}
	else
	{
		buffer.applyGain(1 / *p_ceilingf);
	}

	if (m_ioversample > 1)
	{
		overSampleZS(&buffer, m_bufferResized, numchans);

		m_bufferResized->applyGain(m_ioversample);

		f.process(m_bufferResized->getNumSamples(), m_bufferResized->getArrayOfWritePointers());

		clipSamples(m_bufferResized, numchans);

		f2.process(m_bufferResized->getNumSamples(), m_bufferResized->getArrayOfWritePointers());

		decimate(m_bufferResized, &buffer, numchans);

		if (*p_ceilingf != m_prevceil)
		{
			buffer.applyGainRamp(0, buffer.getNumSamples(), m_prevceil, *p_ceilingf);
			m_prevceil = *p_ceilingf;
		}
		else
		{
			buffer.applyGain(*p_ceilingf);
		}

		if (m_postClipType == 2)
		{
			for (int i = 0; i < buffer.getNumSamples(); i++)
			{
				for (int j = 0; j < numchans; j++)
				{
					float s = buffer.getSample(j, i);
					buffer.setSample(j, i, hardclip(s));
				}
			}
		}

	}
	else
	{
		clipSamples(&buffer, numchans);

		if (*p_ceilingf != m_prevceil)
		{
			buffer.applyGainRamp(0, buffer.getNumSamples(), m_prevceil, *p_ceilingf);
			m_prevceil = *p_ceilingf;
		}
		else
		{
			buffer.applyGain(*p_ceilingf);
		}
	}


	if (getActiveEditor() != nullptr)
	{
		static_cast<FreeClipAudioProcessorEditor*>(getActiveEditor())->m_avmout->pushBuffer(buffer);
	}


	if (*p_outGainf != m_prevoutGain)
	{
		buffer.applyGainRamp(0, buffer.getNumSamples(), m_prevoutGain, *p_outGainf);
		m_prevoutGain = *p_outGainf;
	}
	else
	{
		buffer.applyGain(*p_outGainf);
	}

	levelMeterSourceOut.measureBlock(buffer);

    for (int i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());
}

//==============================================================================
bool FreeClipAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* FreeClipAudioProcessor::createEditor()
{
    return new FreeClipAudioProcessorEditor (*this);
}

//==============================================================================
void FreeClipAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.

	ScopedPointer<XmlElement> xml(new XmlElement("PluginSettings"));
	xml->setAttribute("ceilingf", (double) *p_ceilingf);
	xml->setAttribute("gainf", (double) *p_gainf);
	xml->setAttribute("outGainf", (double) *p_outGainf);
	xml->setAttribute("wavetype", (int) *p_wavetype);
	xml->setAttribute("oversample", (int) *p_oversample);
	xml->setAttribute("postClipType", (int)m_postClipType);

	copyXmlToBinary(*xml, destData);

}

void FreeClipAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
	ScopedPointer<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));
	if (xmlState != nullptr)
		if (xmlState->hasTagName("PluginSettings"))
		{
			*p_ceilingf = xmlState->getDoubleAttribute("ceilingf", 1.0);
			*p_gainf = xmlState->getDoubleAttribute("gainf", 1.0);
			*p_outGainf = xmlState->getDoubleAttribute("outGainf", 1.0);
			*p_wavetype = xmlState->getIntAttribute("wavetype");
			*p_oversample = xmlState->getIntAttribute("oversample", 1);
			m_postClipType = xmlState->getIntAttribute("postClipType", 1);
		}
	m_settingsLoaded = true;
}

float FreeClipAudioProcessor::calcCutoff()
{
	return (m_fsamplerate / 2)*0.98;
	//return 1000.0;
}

void FreeClipAudioProcessor::clipSamples(AudioSampleBuffer* buffer, int numchans)
{
	for (int j = 0; j < numchans; j++)
	{
		float *bufferData = buffer->getWritePointer(j);

		for (int i = 0; i < buffer->getNumSamples(); i++)
		{
			float newval = 0.0f;
			if (*p_wavetype == 0)
				newval = hardclip(bufferData[i]);
			else if (*p_wavetype == 1)
				newval = quintic(bufferData[i]);
			else if (*p_wavetype == 2)
				newval = cubicBasic(bufferData[i]);
			else if (*p_wavetype == 3)
				newval = tanclip(bufferData[i], m_softness);
			else if (*p_wavetype == 4)
				newval = algclip(bufferData[i], m_softness);
			else if (*p_wavetype == 5)
				newval = arcClip(bufferData[i], m_softness);
			bufferData[i] = newval;
		}
	}
}

void FreeClipAudioProcessor::overSampleZS(AudioSampleBuffer * oldBuffer, AudioSampleBuffer * newBuffer, int numchans)
{
	for (int i = 0; i < oldBuffer->getNumSamples(); i++)
	{
		for (int j = 0; j < numchans; j++)
		{
			newBuffer->setSample(j, i * m_ioversample, oldBuffer->getSample(j, i));
		}
	}
}

void FreeClipAudioProcessor::decimate(AudioSampleBuffer * upBuffer, AudioSampleBuffer * downBuffer, int numchans)
{
	for (int i = 0; i < downBuffer->getNumSamples(); i++)
	{
		for (int j = 0; j < numchans; j++)
		{
			if (m_postClipType == 3)
			{
				float s = upBuffer->getSample(j, i * m_ioversample + (int)(sampleShift*(m_ioversample - 1)));
				downBuffer->setSample(j, i, hardclip(s));
			}
			else
			{
				downBuffer->setSample(j, i, upBuffer->getSample(j, i * m_ioversample + (int)(sampleShift*(m_ioversample - 1))));
			}
		}
	}
}

float FreeClipAudioProcessor::sinclip(float &s)
{
	if (fabs(s) < M_PI)
	{
		return sin(s);
	}
	else
	{
		return sgn(s)*1.0f;
	}
}

float FreeClipAudioProcessor::logiclip(float &s)
{
	return 2.0f / (1.0f + exp(-2.0f * s)) - 1.0f;
}

float FreeClipAudioProcessor::hardclip(float &s)
{
	return sgn(s)*fmin(fabs(s), 1.0f);
}

float FreeClipAudioProcessor::tanclip(float &s, float &soft)
{
	return tanh((1.0f - 0.5f*soft)*s);
}

float FreeClipAudioProcessor::quintic(float &s)
{
	if (fabs(s) < 1.25f)
		return s - (256.0f / 3125.0f)*powf(s, 5.0f);
	else
		return sgn(s)*1.0f;
}

float FreeClipAudioProcessor::cubicBasic(float &s)
{
	if (fabs(s) < 1.5f)
		return s - (4.0f / 27.0f)*powf(s, 3.0f);
	else
		return sgn(s)*1.0f;
}

float FreeClipAudioProcessor::algclip(float &s, float soft)
{
	return s / sqrtf((1.0f + 2.0f * soft + powf(s, 2.0f)));
}

float FreeClipAudioProcessor::arcClip(float &s, float &soft)
{
	return (2.0f / M_PI)*atan((1.6f - soft*0.6f)*s);
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new FreeClipAudioProcessor();
}
